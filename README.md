## License

This work is distributed under the terms of the CERN Open Hardware Licence Version 2 - Strongly Reciprocal.

`SPDX-License-Identifier: CERN-OHL-S-2.0`
